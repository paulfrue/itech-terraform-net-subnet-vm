# Terraform installieren
https://learn.hashicorp.com/terraform/getting-started/install.html

# Terraform "Getting started"
https://learn.hashicorp.com/terraform?track=azure#azure

# Subscription, Tenant und Client-Credentials erzeugen
https://docs.microsoft.com/de-de/azure/virtual-machines/linux/terraform-install-configure

# Praxisbeispiel AWS
https://m.heise.de/developer/artikel/Terraform-in-der-Praxis-LAMP-Stack-in-der-Cloud-3961117.html?seite=all

# Übungen
## Ausführen des Beispiels
## Umzug der Instanz in eine andere Location
## Erzeugen einer zweiten VM im gegebenen Subnet
## Transformation des Beispiels zu einer Windwos VM

